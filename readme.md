# Simple Contact Form

A simple BackEnd system to recieve information from a contact form.

## Requirements

* Python3

## Run

Execute the command `pip3 install flask`, then `python3 main.py`

## Credits

Make by [Scidroid](https://scidroid.live/)
