from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == 'GET':
        return 'Hola Mundo por GET'
    else:
        return 'Hola Mundo por POST'

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8080)
